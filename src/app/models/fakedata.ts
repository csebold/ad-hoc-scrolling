import { random, name, internet } from 'faker';

export class Fakedata {
  Name: string = `${name.lastName()} ${random.word()}`;
  Description: string = random.words(random.number(4) + 1);
  SharePrice: number = random.number({ min: .01, max: 1000, precision: 2 });
  NumberOfShares: number = (random.number(25) + 1) * 50;
  Price: number = this.NumberOfShares * this.SharePrice;
  PercentageChange: number = random.number(20) * .01 * (random.number(1) == 1 ? 1 : -1);
}
