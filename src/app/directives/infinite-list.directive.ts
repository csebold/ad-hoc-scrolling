import { Directive, ElementRef, Input } from '@angular/core';
import { InfiniteListService } from '../services/infinite-list.service';

@Directive({
  selector: '[appInfiniteList]'
})
export class InfiniteListDirective {
  @Input() isLast: boolean;
  @Input() appInfiniteList: string;
  constructor(private el: ElementRef, private ilSvc: InfiniteListService) {
    if (window.addEventListener) {
      for (let x of ['DOMContentLoaded', 'load', 'scroll', 'resize']) {
        window.addEventListener(x, this._handler(el), false);
      }
    }
  }

  private _handler(el: ElementRef) {
    return () => {
      if (this.isLast) {
        const rect = el.nativeElement.getBoundingClientRect();
        const height = (screen && screen.height) || window.innerHeight || document.documentElement.clientHeight;
        const debug = {
          rectTop: rect.top,
          windowHeight: window.innerHeight,
          clientHeight: document.documentElement.clientHeight,
          screenHeight: screen ? screen.height : null
        };
        if (rect.top <= height) {
          this.ilSvc.UpdateHitBottom(this.appInfiniteList, true, debug);
        } else {
          this.ilSvc.UpdateHitBottom(this.appInfiniteList, false, debug);
        }
      }
    }
  }
}
