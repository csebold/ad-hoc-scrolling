import { Component, OnInit, OnDestroy } from '@angular/core';
import { InfiniteListService } from './services/infinite-list.service';
import { Fakedata } from './models/fakedata';
import { Subscription } from 'rxjs';

const NUMDATAPOINTS = 30;
const SCROLLADDPOINTS = 20;
const SCROLLER = 'testComponent';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'ad-hoc-scrolling';
  userData = {
    name: 'Abraham Lincoln',
    phone: '5556789123',
    email: 'alincoln@example.com'
  }
  data: Fakedata[] = this.GenerateFakeData(NUMDATAPOINTS);
  private _subscription = new Subscription();
  debugStats: { [index: string]: number } = {};

  GenerateFakeData(numPoints: number): Fakedata[] {
    let out: Fakedata[] = [];
    for (let x = 0; x < numPoints; x++) {
      out.push(new Fakedata());
    }
    return out;
  }

  ngOnInit() {
    this._subscription.add(this.ilSvc.GetHitBottom$(SCROLLER).subscribe(hb => {
      if (hb && this.data.length < 140) {
        for (let x of this.GenerateFakeData(SCROLLADDPOINTS)) {
          this.data.push(x);
        }
      }
    }));
    this._subscription.add(this.ilSvc.Debug$.subscribe(d => this.debugStats = d));
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  constructor(private ilSvc: InfiniteListService) {

  }
}
