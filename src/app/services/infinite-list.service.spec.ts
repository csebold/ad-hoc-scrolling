import { TestBed } from '@angular/core/testing';

import { InfiniteListService } from './infinite-list.service';

describe('InfiniteListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfiniteListService = TestBed.get(InfiniteListService);
    expect(service).toBeTruthy();
  });
});
