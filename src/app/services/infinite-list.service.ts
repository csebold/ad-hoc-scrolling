import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InfiniteListService {

  private _hitBottom: { [index: string]: BehaviorSubject<boolean> } = {};

  private _debug = new BehaviorSubject<{ [index: string]: number }>({});
  Debug$ = this._debug.asObservable();
  constructor() { }

  UpdateHitBottom(scroller: string, value: boolean, debugObject?: { [index: string]: number }) {
    if (!this._hitBottom[scroller]) {
      this._hitBottom[scroller] = new BehaviorSubject<boolean>(value);
    } else if (this._hitBottom[scroller].value !== value) {
      this._hitBottom[scroller].next(value);
    }
    if (debugObject) {
      this._debug.next(debugObject);
    }
  }

  GetHitBottom$(scroller: string) {
    if (!this._hitBottom[scroller]) {
      this._hitBottom[scroller] = new BehaviorSubject<boolean>(false);
    }
    return this._hitBottom[scroller].asObservable();
  }
}
